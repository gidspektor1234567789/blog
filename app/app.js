const express = require('express')
const app = express()
const port = 3000
const fs = require('fs')

app.use(express.static('public'))

app.get('/', (req, res) => {
    fs.readFile('./views/index.html', null, function (error, data) {
        if (error) {
            res.writeHead(404);
            res.write('File not found!');
        } else {
            res.write(data);
        }
        res.end();
    });
})

app.listen(port, () => {console.log('watching you')})