class blogBox extends HTMLElement{
    constructor() {
        super();
        const shadow = this.attachShadow({mode: 'open'});

        const template = document.createElement('template');
        template.innerHTML = 
        '<div class=\'content\'>\
            <h2><slot name=\'blog-title\'></slot></h2> \
            <p><slot name=\'blog-content\'></slot></p> \
            <h1><slot name=\'info\'></slot></h1>\
        </div>'

        const style = document.createElement('style');
        style.textContent = 
        '.content { \
            height: 200px; \
            background: rgb(170, 66, 39);\
            opacity: 0.9;\
            width: 60%;\
            text-align: center;\
            margin: 2%;\
        }\
        .content:hover { \
            background: black;\
        }\
        .content:hover p {\
            display: none;\
        }\
        h1 {\
            color:rgb(170, 66, 39);\
        }\
        h2 {\
            padding: 1%;\
            width: 70%;\
            margin: 0 auto;\
        }\
        p {\
            margin: 1%;\
            color: whitesmoke;\
        }'
        
        shadow.appendChild(style)
        shadow.appendChild(template.content.cloneNode(true));
    }
}

customElements.define('blog-box', blogBox)