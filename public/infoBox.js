class infoBox extends HTMLElement {
    constructor(){
        super();
        const shadow = this.attachShadow({mode: 'open'});

        const template = document.createElement('template');
        template.innerHTML = 
        '<div class=\'info\'>\
            <slot><h4>Wall info</h4></slot> \
            <slot><p>For more information about walls join our monthly meetups. The purposes of the walls in buildings are to support roofs, floors and ceilings; to enclose a space as part of the building envelope along with a roof to give buildings form; and to provide shelter and security. In addition, the wall may house various types of utilities such as electrical wiring or plumbing. Wall construction falls into two basic categories: framed walls or mass-walls.</p></slot> \
        </div>'

        this.addEventListener('click', () => {
            template = document.getElementById('change').innerHTML =
            '<div class=\'info\'>\
                <slot><h4>Destroy all walls</h4></slot> \
                <slot><p>For more information about how to destroy walls join our monthly meetups.</p></slot> \
            </div>'
            })
      
        const style = document.createElement('style');
        style.textContent = 
        '.info { \
            height: 400px; \
            background: rgb(170, 66, 39);\
            opacity: 0.9;\
            width: 30%;\
            text-align: center;\
            top: 15%; \
            right: 1%;\
            cursor: pointer;\
            position: absolute;\
        }\
        h3 {\
            color:rgb(170, 66, 39);\
        }\
        h4 {\
            width: 70%;\
            margin: 0 auto;\
        }\
        p {\
            margin: 1%;\
            color: whitesmoke;\
        }'

        shadow.appendChild(style);
        shadow.appendChild(template.content.cloneNode(true));
    }
}

customElements.define('info-box', infoBox)